﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Models
{
    public class PolicyModel
    {
        [Required]
        public string Apikey { get; set; }
     
        [Required] //PONL
        public string PolicyNo { get; set; }
    
        [Required] //NEW/RENEWAL/EXPIRED
        public string BizOption { get; set; }
      
        public DateTime? BillingDate { get; set; }
   
      
        public DateTime? StartDate { get; set; }
        [Required]
        public DateTime? EndDate { get; set; }
     
        [Required]
        public decimal? SumInsured { get; set; }
        [Required]
        public decimal? GrossPremium { get; set; }
   
      
        [Required] //certificateno
        public string BrokerPortalId { get; set; }

        public InsuredClientModel insuredClient { get; set; }
        public VehicledetailsModel vehicledetails { get; set; }
    }
}
