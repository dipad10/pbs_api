﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Models
{
    public class VehicledetailsModel
    {
     
        public string Vehiclemake { get; set; }
        public string Vehiclemodel { get; set; }

        public string Vehicleyear { get; set; }
        public string colour { get; set; }

        public string Chasisnumber { get; set; }

        public string Enginenumber { get; set; }

        public string Registrationnumber { get; set; }

        public DateTime? Dateofregistration { get; set; }

        public decimal SumInsured { get; set; }

        public decimal Premiumamount { get; set; }


    }
}
