﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Models
{
    public class CreateApi_Model
    {
        [Required]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
    }
}
