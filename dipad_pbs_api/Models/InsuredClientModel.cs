﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Models
{
    public class InsuredClientModel
    {
        //insureddetails
        [Required]
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string OtherNames { get; set; }
        public string Occupation { get; set; }
        public string Address { get; set; }
        [Required]
        public string MobilePhone { get; set; }
        public string LandPhone { get; set; }
        [Required]
        public string Email { get; set; }
        public string Fax { get; set; }
        [Required]//GUID
        public string Remarks { get; set; }
    


    }
}
