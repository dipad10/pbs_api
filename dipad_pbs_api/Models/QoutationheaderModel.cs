﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Models
{
    public class QoutationheaderModel
    {
        public string QoutationNo { get; set; }
        public DateTime? QoutationDatetime { get; set; }
        public string QoutationCreator { get; set; }
        public string ClientId { get; set; }
        public string MktStaffId { get; set; }
        public string Branch { get; set; }
        public string Agent { get; set; }
        public bool? PackagePolicy { get; set; }
        public string ApplicableRateType { get; set; }
        public bool? HealthFloaterPolicy { get; set; }
        public bool? TravelPolicy { get; set; }
        public int? PolicyDuration { get; set; }
        public DateTime? Effectivate { get; set; }
        public DateTime? Expirydate { get; set; }
        public bool DebitNote { get; set; }
     
        public int BusinessSource { get; set; }
        public double CommissionRate { get; set; }
  
        public string Policyno { get; set; }
    
        public string DebitNoteNumber { get; set; }
    
        public bool RenewalPolicy { get; set; }
       
        public double Exchangerate { get; set; }
        public string CoinsuranceType { get; set; }
        public double Cession { get; set; }
       
        public bool PolicyCloned { get; set; }
    
        public decimal PolicySumInsured { get; set; }
     
        public string PortalName { get; set; }
     
        public bool OveriddenToProduction { get; set; }
     
        public string QouteEmailAddress { get; set; }
      
        public bool PolicyPrinted { get; set; }
     
        public bool SendPolicyByEmail { get; set; }
    }
}
