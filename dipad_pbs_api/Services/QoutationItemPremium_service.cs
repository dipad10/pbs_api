﻿using Dipad_PBS_API.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Services
{
    public class QoutationItemPremium_service
    {

        Gibs5db_pap_TestContext _context = new Gibs5db_pap_TestContext();

        public QoutationItemsPremium Insertquotationitems(string quotationno, decimal? amount)
        {
            SystemDefault_Service service = new SystemDefault_Service();
            QoutationItemsPremium quote = new QoutationItemsPremium
            {
               QoutationNo = quotationno,
               PackageCoverTypeSequenceid = 0,
               CoverTypeSequenceid = 71,
               CoverTypeIndicatorId = 245,
               Rate1 = 0,
               Rate2 = 0,
               Rate3 = 0,
               Doublerate = "",
               NumberOfUnits = 1,
               SumInsured = amount,
               PremiumAmount = amount,
               InsuredMemberId = 0,
               InsuredDependent = 0,
               MemberSequence = 0,
               InsuredName ="",
               InsuredAge = 0,
               StaffId = "",
               DateTimeModified = DateTime.Now,
               RiskGroup = "",
               RiskTableCategory = "",
               ExcessAmount = 0,
               ExcessPercentage = 0,
               ExchangeRate = 0,
               CovertedAmount = 0,
               QouteGroupIndicatorId = 0,
               Tppdunits = 0,
               TppdAllowedAmount = 0,
               Endorsement = false,
               EndorsementApproved = false,

               ShortPeriod = false,
               RemainingPeriod = 0

            };
            _context.QoutationItemsPremium.Add(quote);
            _context.SaveChanges();
            return quote;

        }
    }
}
