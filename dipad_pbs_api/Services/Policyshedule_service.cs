﻿using Dipad_PBS_API.Data;
using Dipad_PBS_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Services
{
    public class Policyshedule_service
    {
        Gibs5db_pap_TestContext _context = new Gibs5db_pap_TestContext();

        //get all quotation
        public IEnumerable<PolicySchedule> Getpolicyshedule()
        {

            return _context.PolicySchedule.OrderByDescending(p => p.SequenceId).ToList();

        }

     

        public PolicySchedule Insertpolicyshedule(VehicledetailsModel model, string quotationno, string policyno, string certno, string insuredname, DateTime? startdate)
        {
            SystemDefault_Service service = new SystemDefault_Service();
            PolicySchedule schedule = new PolicySchedule

            {
                QoutationNo = quotationno,
                CovertypesequenceId = 71,
                Username = "scratchcard",
                Datetimemodified = DateTime.Now,
                A = "1",
                B = service.Getcarmakeid(model.Vehiclemodel),
                C = service.Getcarbrandid(model.Vehiclemodel),
                D = model.Enginenumber,
                E = insuredname,
                F = model.colour,
                G = model.Registrationnumber,
                H = model.Chasisnumber,
                L = "Carriage of Goods for hire or reward,Carriage of own Goods Only,Commercial,Motor Trade,Private,Private Passenger Hire,Public Passenger Hire,",
                N = "0",
                O = "0",
                P = "0",
                Q = "0",
                CertificateNo = certno,
                Effectivedate = startdate,
                Policyno = policyno,
                NiidUploaded = true,
                Submittedon = DateTime.Now,

            };
            _context.PolicySchedule.Add(schedule);
            _context.SaveChanges();
            return schedule;

        }
    }
}
