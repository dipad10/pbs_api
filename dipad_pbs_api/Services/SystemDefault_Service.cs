﻿using Dipad_PBS_API.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Services
{
    public class SystemDefault_Service
    {
        Gibs5db_pap_TestContext _context = new Gibs5db_pap_TestContext();

        public long? GetInsurednumber()
        {
            var Insuredid = _context.AutoNumbers.Where(p => p.NumType == "insuredno").FirstOrDefault();
            return Insuredid.NextValue;
        }

        public void UpdateInsuredno()
        {
            try
            {
                var Insuredid = _context.AutoNumbers.Where(p => p.NumType == "insuredno").FirstOrDefault();

                Insuredid.NextValue = Insuredid.NextValue + 1;

                _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
         

        }

        public long? Getdebitnotenumber()
        {
            var debitnotenum = _context.AutoNumbers.Where(p => p.NumType == "debitnoteno").FirstOrDefault();
            return debitnotenum.NextValue;
        }

        public long? Getquotationo()
        {
            var quotenum = _context.AutoNumbers.Where(p => p.NumType == "quotationno").FirstOrDefault();
            return quotenum.NextValue;
        }
        public long? Getreferenceno()
        {
            var quotenum = _context.AutoNumbers.Where(p => p.NumType == "referenceno").FirstOrDefault();
            return quotenum.NextValue;
        }

        public void Updatequotationno()
        {
            var quotenum = _context.AutoNumbers.Where(p => p.NumType == "quotationno").FirstOrDefault();

            quotenum.NextValue = quotenum.NextValue + 1;
            _context.SaveChanges();

        }

        public void Updatereferenceno()
        {
            var quotenum = _context.AutoNumbers.Where(p => p.NumType == "referenceno").FirstOrDefault();

            quotenum.NextValue = quotenum.NextValue + 1;
            _context.SaveChanges();

        }
        public void Updatedebitnoteno()
        {
            var debitnotenum = _context.AutoNumbers.Where(p => p.NumType == "debitnoteno").FirstOrDefault();

            debitnotenum.NextValue = debitnotenum.NextValue + 1;
            _context.SaveChanges();

        }
        public string GenerateReinrefno(DateTime? startdate)
        {
            return $"{"RI/TTY/QRT3"}/{Convert.ToDateTime(startdate).ToString("yyyy")}/{Getreferenceno()}";
        }
        public string GenerateFacrefno(DateTime? startdate)
        {
            return $"{"FAC/TTY/QRT3"}/{Convert.ToDateTime(startdate).ToString("yyyy")}/{Getreferenceno()}";
        }

        public string GenerateCessionno(DateTime? startdate)
        {
            return $"{"TTY/QRT3"}/{Convert.ToDateTime(startdate).ToString("yyyy")}/{Getreferenceno()}";
        }

        public string GenerateDNCNno(DateTime? startDate, DateTime? endDate)
        {
            return $"{"DNEB"}{Convert.ToDateTime(startDate).ToString("dd")}{Convert.ToDateTime(endDate).ToString("yy")}{Getdebitnotenumber()}";
        }

        public string GenerateQuotationNo(DateTime? startdate, DateTime? enddate)
        {
            return $"{"EB"}{Convert.ToDateTime(startdate).ToString("dd")}{Convert.ToDateTime(enddate).ToString("yyyy")}{Getquotationo()}";
        }

        public string Getcarmakeid(string vehiclemodel)
        {
            var vehicle = _context.VehMakeBrands.Where(p => p.Brand == vehiclemodel).FirstOrDefault();
            return Convert.ToString(vehicle.MakeId);

        }

        public string Getcarbrandid(string vehiclemodel)
        {
            var vehicle = _context.VehMakeBrands.Where(p => p.Brand == vehiclemodel).FirstOrDefault();
            return Convert.ToString(vehicle.BrandId);

        }
    }
}
