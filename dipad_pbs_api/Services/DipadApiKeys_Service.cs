﻿using Dipad_PBS_API.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Services
{
    public class DipadApiKeys_Service
    {

         Gibs5db_pap_TestContext _context = new Gibs5db_pap_TestContext();
      
        public IEnumerable<DipadApiKeys> GetKeys()
        {
            return _context.DipadApiKeys.OrderByDescending(p => p.Submittedon).ToList();
        }
        //togetkey
        public DipadApiKeys ValidateKey(string apikey)
        {
            return _context.DipadApiKeys.FirstOrDefault(p => p.ApiKey == apikey);
        }

        //GenerateKey
        public string Generatekey()
        {
            try
            {
                DipadApiKeys apikeys = new DipadApiKeys();
                apikeys.ApiKey = Guid.NewGuid().ToString();
                apikeys.Submittedby = Config.Configuration.GetValue<string>("appsettings:Username");
                apikeys.Submittedon = DateTime.Now;
                apikeys.Expiry = DateTime.Now.AddDays(30);
                apikeys.Active = 1;
                _context.DipadApiKeys.Add(apikeys);
                _context.SaveChanges();


                return apikeys.ApiKey;
            }
            catch (Exception)
            {

                throw;
            }
         
        }
    }
}
