﻿using Dipad_PBS_API.Data;
using Dipad_PBS_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Services
{
    public class QuotationHeader_service
    {
        Gibs5db_pap_TestContext _context = new Gibs5db_pap_TestContext();

        //get all quotation
        public IEnumerable<QoutationHeader> GetQoutations()
        {

            return _context.QoutationHeader.OrderByDescending(p => p.QoutationId).ToList();

        }

       

        public QoutationHeader InsertQuotation(QoutationheaderModel data)
        {
            var systemservice = new SystemDefault_Service();
            QoutationHeader quote = new QoutationHeader
            {
                QoutationNo = new SystemDefault_Service().GenerateQuotationNo(data.Effectivate, data.Expirydate),
                QoutationDatetime = data.QoutationDatetime,
                QoutationCreator = data.QoutationCreator,//certno
                ClientId = data.ClientId,//insuredid
                MktStaffId = "scratchcard",
                Branch = "14",
                Agent = "01",
                PackagePolicy = false,
                ApplicableRateType = "Single",
                HealthFloaterPolicy = false,
                TravelPolicy = false,
                PolicyDuration = 364,
                Effectivate = data.Effectivate,
                Expirydate = data.Expirydate,
                DebitNote = true,
                BusinessSource = 3,
                CommissionRate = 0,
                Policyno = data.Policyno,
                DebitNoteNumber = data.DebitNoteNumber,
                RenewalPolicy = false,
                Exchangerate = 1,
                CoinsuranceType = "leading",
                Cession = 100,
                PolicyCloned = false,
                PolicySumInsured = 0
            };

            _context.QoutationHeader.Add(quote);
            _context.SaveChanges();
            systemservice.Updatequotationno();
            return quote;
            

        }


    }
}
