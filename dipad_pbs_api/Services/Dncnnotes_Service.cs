﻿using Dipad_PBS_API.Data;
using Dipad_PBS_API.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Services
{
    public class Dncnnotes_Service
    {
        Gibs5db_pap_TestContext _context = new Gibs5db_pap_TestContext();
       

        //get all dncnnotes
        public IEnumerable<Dncnnotes> GetDncnnotes()
        {

            return _context.Dncnnotes.OrderByDescending(p => p.Dncnid).ToList();

        }
        //post dncn
        public Dncnnotes Insertdncnote(PolicyModel data, string insuredid, string insuredname)
        {
            
            Dncnnotes rec = new Dncnnotes();

            var systemdefault = new SystemDefault_Service();

            //save data to dncn

            rec.Dncnno = systemdefault.GenerateDNCNno(data.StartDate, data.EndDate);
            rec.RefDncnno = rec.Dncnno;
            rec.PolicyNo = data.PolicyNo;
            rec.BranchId = "14";
            rec.BizSource = "BROKER";
            rec.BizOption = data.BizOption;
            rec.NoteType = "DN";
            rec.BillingDate = data.StartDate;
            rec.SubRiskId = "PRMITP";
            rec.SubRisk = "Private Cars Individual Third Party";
            rec.PartyId = "1";
            rec.Party = "Direct";
            rec.InsuredId = insuredid;
            rec.InsuredName = insuredname;
            rec.StartDate = data.StartDate;
            rec.EndDate = data.EndDate;
            rec.SumInsured = data.SumInsured;
            rec.GrossPremium = data.GrossPremium;
            rec.Commission = 0;
            rec.VatAmount = 0;
            rec.ExRate = 1;
            rec.ExCurrency = "NAIRA";
            rec.PaymentType = "Normal";
            rec.Deleted = 0;
            rec.Active = 1;
            rec.PremiumCollected = data.GrossPremium;
            rec.BrokerPortalId = data.BrokerPortalId; //certificateno
            rec.SyncDatetime = DateTime.Now;
            rec.ReceiptStatus = true;
            rec.PostToFinance = true;
            rec.RetentionPerc = 1;
         
            _context.Dncnnotes.Add(rec);
            _context.SaveChanges();
            //update debitnote autonumber
            systemdefault.Updatedebitnoteno();

            return rec;
            //save quotation header
         
            //save policyshedule(vehicledetails) here
         



        }
    }
}
