﻿using Dipad_PBS_API.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Services
{
    public class QuotationRiskDistribution_service
    {

        Gibs5db_pap_TestContext _context = new Gibs5db_pap_TestContext();

        public QoutationRiskDistributions Insertquotationrisk(string policyno, string quotationno, decimal? amount, DateTime? startdate)
        {
            SystemDefault_Service service = new SystemDefault_Service();
            QoutationRiskDistributions quote = new QoutationRiskDistributions
            {
                PolicyNo = policyno,
               QoutationNo = quotationno,
               PolicyTypeId = 71,
               DatetimeModified = DateTime.Now,
               Username = "AUTO",
               QoutaShare = 0,
               QoutaShareAmount = 0,
               RetentionAmount = amount,
               RetentionPercentage = 1,
               Lines = 0,
               Surplus = 0,
               Facultative = 0,
               FacultativePercentage = 0,
               Topmostlocation = amount,
               CommissionPercentage = 0,
               Vatpercentage = 0,
               Remarks = "",
               ModifiedOn = DateTime.Now,
               GrossPremium = 0,
               ClaimPaidAmount = 0,
               ClaimAdjustorFeePaid = 0,
               ManualProrate = false,
               CessionNumber = service.GenerateCessionno(startdate),
               FacReferenceNumber = service.GenerateFacrefno(startdate),
               ReinsuranceReferenceNumber = service.GenerateReinrefno(startdate),
               GrossPremiumBorderaux = 0
               
               
            };
            _context.QoutationRiskDistributions.Add(quote);
            _context.SaveChanges();
            service.Updatereferenceno(); 
            return quote;

        }
    }
}
