﻿using Dipad_PBS_API.Data;
using Dipad_PBS_API.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dipad_PBS_API.Services
{
    public class InsuredClients_Service
    {
        Gibs5db_pap_TestContext _context = new Gibs5db_pap_TestContext();

        public InsuredClients Insertinsuredclient(InsuredClientModel insured)
        {
            try
            {
                var client = new InsuredClients();
                var systemservice = new SystemDefault_Service();
                client.InsuredId = Convert.ToString(systemservice.GetInsurednumber());
                client.Surname = insured.Surname;
                client.Occupation = insured.Occupation;
                client.Address = insured.Address;
                client.MobilePhone = insured.MobilePhone;
                client.Remarks = insured.Remarks;
                client.Email = insured.Email;

                client.Dob = DateTime.Now;
                client.Deleted = 0;
                client.Active = 1;
                client.AgentId = "01";
                client.MktStaffId = "scratchcard";
                client.TopRisk = false;
                client.TakeOnBalance = 000;

                _context.InsuredClients.Add(client);
                _context.SaveChanges();
                //update autonumber
                systemservice.UpdateInsuredno();
                return client;

            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        public InsuredClients GetInsured(string remarks)
        {
            return _context.InsuredClients.Where(p => p.Remarks == remarks).FirstOrDefault();

        }
    }
}
