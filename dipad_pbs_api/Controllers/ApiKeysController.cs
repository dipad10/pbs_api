﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dipad_PBS_API.Data;
using Dipad_PBS_API.Models;
using Dipad_PBS_API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Dipad_PBS_API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ApiKeysController : ControllerBase
    {
        private readonly IConfiguration _configuration;
       
       public ApiKeysController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
       
        [HttpGet("{username}/{password}")]
        public IActionResult GetApikey(string username, string password)
        {
            if (ModelState.IsValid)
            {
                var apiusername = _configuration.GetValue<string>("appsettings:Username");
                var apipassword = _configuration.GetValue<string>("appsettings:Password");
                //check if username and password is valid to generate apikey
                if (username != apiusername || password != apipassword)
                {
                    return BadRequest("Invalid username or password");
                }
                var apikey = new DipadApiKeys_Service().Generatekey();
                return Ok(apikey);
            }
            else
            {
                return BadRequest(ModelState);
            }
       

        }

      
        [HttpGet("{username}/{password}")]
        public IActionResult GetApikeyLists(string username, string password)
        {
            var apiusername = _configuration.GetValue<string>("appsettings:Username");
            var apipassword = _configuration.GetValue<string>("appsettings:Password");
            //check if username and password is valid to list all keys
            if (username != apiusername || password != apipassword)
            {
                return BadRequest("Invalid username or password");
            }
            var res = new DipadApiKeys_Service().GetKeys();
            return Ok(res);
        }
    }
}