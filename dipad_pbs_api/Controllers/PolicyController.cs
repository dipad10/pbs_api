﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dipad_PBS_API.Data;
using Dipad_PBS_API.Models;
using Dipad_PBS_API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;
namespace Dipad_PBS_API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PolicyController : ControllerBase
    {
        private readonly ILogger _logger;

        public PolicyController(ILogger logger)
        {
            _logger = logger;
        }
        [HttpGet("{apikey}")]
        public IActionResult GetPolicies(string apikey)
        {
            if(ModelState.IsValid)
            {
                _logger.Info($"Validating key... {apikey} ");
                var key = new DipadApiKeys_Service().ValidateKey(apikey);
                if (key == null)
                {
                    _logger.Info("Apikey is invalid, could not complete user request!");
                    return BadRequest("Apikey is invalid");
                }
                var policies = new Dncnnotes_Service().GetDncnnotes();
                return Ok(policies);
            }
            else
            {
                return BadRequest(ModelState);
            }
           
       
        }

        [HttpPost]
        public IActionResult PostPolicy([FromBody] PolicyModel model)
        {
            if (ModelState.IsValid)
            {
                _logger.Info($"Validating key... {model.Apikey}");
                var key = new DipadApiKeys_Service().ValidateKey(model.Apikey);
                if (key == null) {
                    _logger.Info("Apikey is invalid, could not complete user request!");
                    return BadRequest("Api key is invalid");
                }
                var response = new Policy_service().Insertpolicy(model);
                if (response == 0) {
                    //successful
                    _logger.Info("Data saved successfully for Policy {0}", model.PolicyNo);
                    return Ok("Data posted successfully");
                }
                else{
                    return StatusCode(500);
                }
            }
            else {
                return BadRequest(ModelState);
            }
         
            

        }

    }
}