﻿using System;
using System.Collections.Generic;

namespace Dipad_PBS_API.Data
{
    public partial class QoutationRiskDistributions
    {
        public int SequenceId { get; set; }
        public string QoutationNo { get; set; }
        public int? PolicyTypeId { get; set; }
        public DateTime? DatetimeModified { get; set; }
        public string Username { get; set; }
        public double? QoutaShare { get; set; }
        public decimal? QoutaShareAmount { get; set; }
        public decimal? RetentionAmount { get; set; }
        public double? RetentionPercentage { get; set; }
        public int? Lines { get; set; }
        public decimal? Surplus { get; set; }
        public double? SurplusPercentage { get; set; }
        public decimal? Facultative { get; set; }
        public double? FacultativePercentage { get; set; }
        public decimal? Topmostlocation { get; set; }
        public decimal? TotalSumInsured { get; set; }
        public double CommissionPercentage { get; set; }
        public decimal? CommissionAmount { get; set; }
        public double Vatpercentage { get; set; }
        public decimal? Vatamount { get; set; }
        public string Remarks { get; set; }
        public DateTime? ReinsuranceStartDate { get; set; }
        public DateTime? ReinsuranceEndDate { get; set; }
        public string ReinsuranceProcessedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public decimal GrossPremium { get; set; }
        public string PolicyNo { get; set; }
        public string InsuredName { get; set; }
        public string SubClass { get; set; }
        public DateTime? InsuranceStartDate { get; set; }
        public DateTime? InsuranceEndDate { get; set; }
        public double? OurRetentionPercentage { get; set; }
        public decimal? OurRetentionPremiumAmount { get; set; }
        public decimal? FacultativePremiumAmount { get; set; }
        public string ExternalPolicyNo { get; set; }
        public decimal ClaimPaidAmount { get; set; }
        public decimal ClaimAdjustorFeePaid { get; set; }
        public double? ExchangeRate { get; set; }
        public bool ManualProrate { get; set; }
        public string LocationType { get; set; }
        public string RiskClass { get; set; }
        public string FirstLossAmount { get; set; }
        public string LimitPerCarriage { get; set; }
        public string MarineRiskClass { get; set; }
        public string CessionNumber { get; set; }
        public string FacReferenceNumber { get; set; }
        public string ReinsuranceReferenceNumber { get; set; }
        public string ScheduleType { get; set; }
        public decimal GrossPremiumBorderaux { get; set; }
        public string SubRiskClass { get; set; }
    }
}
