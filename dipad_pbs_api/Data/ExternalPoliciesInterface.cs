﻿using System;
using System.Collections.Generic;

namespace Dipad_PBS_API.Data
{
    public partial class ExternalPoliciesInterface
    {
        public int Sequenceid { get; set; }
        public string PolicyNumber { get; set; }
        public string MainClassCode { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public DateTime Submmittedon { get; set; }
        public string Xmldocument { get; set; }
        public string PortalName { get; set; }
        public string EmailAddress { get; set; }
        public int? InsuredId { get; set; }
    }
}
