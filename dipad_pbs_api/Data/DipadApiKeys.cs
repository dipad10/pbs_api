﻿using System;
using System.Collections.Generic;

namespace Dipad_PBS_API.Data
{
    public partial class DipadApiKeys
    {
        public int Keyid { get; set; }
        public string ApiKey { get; set; }
        public DateTime? Submittedon { get; set; }
        public string Submittedby { get; set; }
        public int? Active { get; set; }
        public DateTime? Expiry { get; set; }
    }
}
