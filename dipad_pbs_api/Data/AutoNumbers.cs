﻿using System;
using System.Collections.Generic;

namespace Dipad_PBS_API.Data
{
    public partial class AutoNumbers
    {
        public long NumId { get; set; }
        public string NumType { get; set; }
        public string BranchId { get; set; }
        public string RiskId { get; set; }
        public long? NextValue { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string Format { get; set; }
        public string Sample { get; set; }
    }
}
