﻿using System;
using System.Collections.Generic;

namespace Dipad_PBS_API.Data
{
    public partial class PolicySchedule
    {
        public int SequenceId { get; set; }
        public string QoutationNo { get; set; }
        public int CovertypesequenceId { get; set; }
        public string Username { get; set; }
        public DateTime Datetimemodified { get; set; }
        public string DeletedReference { get; set; }
        public string A { get; set; }
        public string B { get; set; }
        public string C { get; set; }
        public string D { get; set; }
        public string E { get; set; }
        public string F { get; set; }
        public string G { get; set; }
        public string H { get; set; }
        public string I { get; set; }
        public string J { get; set; }
        public string K { get; set; }
        public string L { get; set; }
        public string M { get; set; }
        public string N { get; set; }
        public string O { get; set; }
        public string P { get; set; }
        public string Q { get; set; }
        public string R { get; set; }
        public string S { get; set; }
        public string T { get; set; }
        public string U { get; set; }
        public string V { get; set; }
        public string W { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string Z { get; set; }
        public string CertificateNo { get; set; }
        public bool Endorsement { get; set; }
        public bool EndorsementApproved { get; set; }
        public string Endorsementno { get; set; }
        public DateTime? Effectivedate { get; set; }
        public string Policyno { get; set; }
        public bool NiidUploaded { get; set; }
        public string NiidUploadedBy { get; set; }
        public DateTime? NiidUploadedOn { get; set; }
        public DateTime? NiidCanceledOn { get; set; }
        public string NiidCanceledBy { get; set; }
        public DateTime Submittedon { get; set; }
    }
}
