﻿using System;
using System.Collections.Generic;

namespace Dipad_PBS_API.Data
{
    public partial class InsuredClients
    {
        public string InsuredId { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string OtherNames { get; set; }
        public string Occupation { get; set; }
        public string Address { get; set; }
        public string MobilePhone { get; set; }
        public string LandPhone { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Remarks { get; set; }
        public string InsuredType { get; set; }
        public string Profile { get; set; }
        public string Contperson { get; set; }
        public DateTime? Dob { get; set; }
        public byte? Deleted { get; set; }
        public byte? Active { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public string Field6 { get; set; }
        public string Field7 { get; set; }
        public string Field8 { get; set; }
        public string Field9 { get; set; }
        public string Field10 { get; set; }
        public decimal? A1 { get; set; }
        public decimal? A2 { get; set; }
        public decimal? A3 { get; set; }
        public decimal? A4 { get; set; }
        public decimal? A5 { get; set; }
        public DateTime? ExtraDate1 { get; set; }
        public DateTime? ExtraDate2 { get; set; }
        public string SubmittedBy { get; set; }
        public DateTime? SubmittedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string MeansId { get; set; }
        public string MeansIdno { get; set; }
        public string AgentId { get; set; }
        public string MktStaffId { get; set; }
        public string Passportno { get; set; }
        public string Gender { get; set; }
        public string AlternativeEmail { get; set; }
        public string Tin { get; set; }
        public bool TopRisk { get; set; }
        public string Password { get; set; }
        public string PortalInsuredId { get; set; }
        public decimal TakeOnBalance { get; set; }
        public string GlCode { get; set; }
    }
}
