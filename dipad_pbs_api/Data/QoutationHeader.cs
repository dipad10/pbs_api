﻿using System;
using System.Collections.Generic;

namespace Dipad_PBS_API.Data
{
    public partial class QoutationHeader
    {
        public int QoutationId { get; set; }
        public string QoutationNo { get; set; }
        public string QoutationCurrency { get; set; }
        public DateTime? QoutationDatetime { get; set; }
        public string QoutationCreator { get; set; }
        public string ClientId { get; set; }
        public string MktStaffId { get; set; }
        public string QoutationStatus { get; set; }
        public DateTime? Datetimemodified { get; set; }
        public string Modifiedby { get; set; }
        public string Branch { get; set; }
        public string Agent { get; set; }
        public bool? PackagePolicy { get; set; }
        public string ApplicableRateType { get; set; }
        public bool? HealthFloaterPolicy { get; set; }
        public bool? TravelPolicy { get; set; }
        public int? PolicyDuration { get; set; }
        public DateTime? Effectivate { get; set; }
        public DateTime? Expirydate { get; set; }
        public bool DebitNote { get; set; }
        public string PolicyScheduleType { get; set; }
        public string PaymentType { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string Approvedby { get; set; }
        public byte[] PolicyClauses { get; set; }
        public string OriginalPolicyNumber { get; set; }
        public string PreviousPolicyNumber { get; set; }
        public int BusinessSource { get; set; }
        public double CommissionRate { get; set; }
        public string LastEndorsementBy { get; set; }
        public DateTime? LastendorsementDate { get; set; }
        public string Policyno { get; set; }
        public string ReceiptedBy { get; set; }
        public DateTime? ReceiptedOn { get; set; }
        public string DebitNoteNumber { get; set; }
        public decimal? Receiptedamount { get; set; }
        public string Receiptnumber { get; set; }
        public bool RenewalPolicy { get; set; }
        public string DeapprovedBy { get; set; }
        public DateTime? Deapprovedon { get; set; }
        public double Exchangerate { get; set; }
        public string CoinsuranceType { get; set; }
        public double Cession { get; set; }
        public string PremiumCollectionType { get; set; }
        public bool PolicyCloned { get; set; }
        public string PolicyClonedFrom { get; set; }
        public string PolicyBizOption { get; set; }
        public decimal PolicySumInsured { get; set; }
        public string RefCreditNoteNumber { get; set; }
        public DateTime? RefCreditNoteDate { get; set; }
        public string PortalName { get; set; }
        public string QouteInsuredName { get; set; }
        public string QouteMobileNumber { get; set; }
        public string CommissionAddedBy { get; set; }
        public DateTime? CommissionAddedOn { get; set; }
        public bool OveriddenToProduction { get; set; }
        public string OveriddenToProductionBy { get; set; }
        public DateTime? OveriddenToProductionOn { get; set; }
        public string QouteEmailAddress { get; set; }
        public string FacinReinsurer { get; set; }
        public bool PolicyPrinted { get; set; }
        public string PolicyPrintedBy { get; set; }
        public bool SendPolicyByEmail { get; set; }
    }
}
