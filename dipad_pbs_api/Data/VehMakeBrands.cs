﻿using System;
using System.Collections.Generic;

namespace Dipad_PBS_API.Data
{
    public partial class VehMakeBrands
    {
        public long BrandId { get; set; }
        public string FullName { get; set; }
        public long MakeId { get; set; }
        public string Make { get; set; }
        public string Brand { get; set; }
    }
}
