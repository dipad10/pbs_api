﻿using System;
using System.Collections.Generic;

namespace Dipad_PBS_API.Data
{
    public partial class SystemDefaults
    {
        public int NextInsuredId { get; set; }
        public string Basecurrency { get; set; }
        public int? NextMotorCertificate { get; set; }
        public int NextMarineCertificate { get; set; }
        public decimal? NextPolicyId { get; set; }
        public int NextClaimNo { get; set; }
        public int? NextDebitNo { get; set; }
        public int NextCreditNo { get; set; }
        public decimal NextPartyId { get; set; }
        public double MaximumCommissionRate { get; set; }
        public double? ReinsuranceCommissionRate { get; set; }
        public string NiidUser { get; set; }
        public string NiidUserPassword { get; set; }
        public decimal TppdDefaultValue { get; set; }
        public int? NextCommissionBatch { get; set; }
        public decimal? NextDuplicateBatch { get; set; }
        public int NextCommissionVatBatch { get; set; }
    }
}
