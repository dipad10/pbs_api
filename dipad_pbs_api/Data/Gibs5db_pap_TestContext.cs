﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace Dipad_PBS_API.Data
{
    public partial class Gibs5db_pap_TestContext : DbContext
    {
        public Gibs5db_pap_TestContext()
        {
        }

        public Gibs5db_pap_TestContext(DbContextOptions<Gibs5db_pap_TestContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AutoNumbers> AutoNumbers { get; set; }
        public virtual DbSet<DipadApiKeys> DipadApiKeys { get; set; }
        public virtual DbSet<Dncnnotes> Dncnnotes { get; set; }
        public virtual DbSet<ExternalPoliciesInterface> ExternalPoliciesInterface { get; set; }
        public virtual DbSet<InsuredClients> InsuredClients { get; set; }
        public virtual DbSet<PolicySchedule> PolicySchedule { get; set; }
        public virtual DbSet<QoutationHeader> QoutationHeader { get; set; }
        public virtual DbSet<QoutationItemsPremium> QoutationItemsPremium { get; set; }
        public virtual DbSet<QoutationRiskDistributions> QoutationRiskDistributions { get; set; }
        public virtual DbSet<SystemDefaults> SystemDefaults { get; set; }
        public virtual DbSet<VehMakeBrands> VehMakeBrands { get; set; }
        public virtual DbSet<VehMakes> VehMakes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Config.Configuration.GetValue<string>("ConnectionStrings:API"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity<AutoNumbers>(entity =>
            {
                entity.HasKey(e => new { e.NumId, e.NumType, e.BranchId })
                    .HasName("PK_Autonumber");

                entity.Property(e => e.NumId)
                    .HasColumnName("NumID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.NumType)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.BranchId)
                    .HasColumnName("BranchID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Format)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdated).HasColumnType("smalldatetime");

                entity.Property(e => e.RiskId)
                    .HasColumnName("RiskID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Sample)
                    .HasMaxLength(64)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DipadApiKeys>(entity =>
            {
                entity.HasKey(e => e.Keyid);

                entity.Property(e => e.ApiKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Expiry).HasColumnType("datetime");

                entity.Property(e => e.Submittedby)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Submittedon).HasColumnType("datetime");
            });

            modelBuilder.Entity<Dncnnotes>(entity =>
            {
                entity.HasKey(e => e.Dncnid)
                    .HasName("PK_DNCNNotes_1");

                entity.ToTable("DNCNNotes");

                entity.Property(e => e.Dncnid).HasColumnName("DNCNID");

                entity.Property(e => e.A1).HasColumnType("money");

                entity.Property(e => e.A10).HasColumnType("money");

                entity.Property(e => e.A2).HasColumnType("money");

                entity.Property(e => e.A3).HasColumnType("money");

                entity.Property(e => e.A4).HasColumnType("money");

                entity.Property(e => e.A5).HasColumnType("money");

                entity.Property(e => e.A6).HasColumnType("money");

                entity.Property(e => e.A7).HasColumnType("money");

                entity.Property(e => e.A8).HasColumnType("money");

                entity.Property(e => e.A9).HasColumnType("money");

                entity.Property(e => e.AdminCharge).HasColumnType("money");

                entity.Property(e => e.AllocatedDncnno)
                    .HasColumnName("allocated_dncnno")
                    .HasMaxLength(255);

                entity.Property(e => e.AmountDisbursed).HasColumnType("money");

                entity.Property(e => e.ApprovedBy)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.ApprovedOn).HasColumnType("smalldatetime");

                entity.Property(e => e.BatchNumber).HasMaxLength(255);

                entity.Property(e => e.BeneficiaryName).HasMaxLength(255);

                entity.Property(e => e.BillingDate).HasColumnType("smalldatetime");

                entity.Property(e => e.BizOption)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.BizSource)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.BranchId)
                    .HasColumnName("BranchID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.BrokerAdviseConverted).HasColumnName("BrokerAdvise_Converted");

                entity.Property(e => e.BrokerAdviseConvertedBy).HasMaxLength(255);

                entity.Property(e => e.BrokerAdviseCovertedOn)
                    .HasColumnName("BrokerAdvise_CovertedOn")
                    .HasColumnType("datetime");

                entity.Property(e => e.BrokerAdviseLinkingReceipt)
                    .HasColumnName("BrokerAdvise_LinkingReceipt")
                    .HasMaxLength(255);

                entity.Property(e => e.BrokerPortalId)
                    .HasColumnName("BrokerPortalID")
                    .HasMaxLength(255);

                entity.Property(e => e.BsCode).HasMaxLength(255);

                entity.Property(e => e.ChequeNo)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.CoPolicyNo)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Commission).HasColumnType("money");

                entity.Property(e => e.CommissionApprovedBy).HasMaxLength(255);

                entity.Property(e => e.CommissionApprovedOn).HasColumnType("datetime");

                entity.Property(e => e.CommissionFinalApprovedBy).HasMaxLength(255);

                entity.Property(e => e.CommissionFinalApprovedOn).HasColumnType("datetime");

                entity.Property(e => e.CommissionPaid).HasColumnType("money");

                entity.Property(e => e.CommissionVatapproved).HasColumnName("CommissionVATApproved");

                entity.Property(e => e.CommissionVatapprovedBy)
                    .HasColumnName("CommissionVATApprovedBy")
                    .HasMaxLength(255);

                entity.Property(e => e.CommissionVatapprovedOn)
                    .HasColumnName("CommissionVATApprovedOn")
                    .HasColumnType("datetime");

                entity.Property(e => e.CommissionVatpaid)
                    .HasColumnName("commissionVATpaid")
                    .HasColumnType("money");

                entity.Property(e => e.CompanyId)
                    .HasColumnName("CompanyID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CoverType)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.CrrefDate)
                    .HasColumnName("CRRefDate")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Dbdate)
                    .HasColumnName("DBDate")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn).HasColumnType("smalldatetime");

                entity.Property(e => e.DisbursementVoucherNo).HasMaxLength(255);

                entity.Property(e => e.Dncnno)
                    .HasColumnName("DNCNNo")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("smalldatetime");

                entity.Property(e => e.Endorsement).HasColumnName("endorsement");

                entity.Property(e => e.EndorsementApproved).HasColumnName("endorsement_approved");

                entity.Property(e => e.EndorsementApprovedon)
                    .HasColumnName("endorsement_approvedon")
                    .HasColumnType("datetime");

                entity.Property(e => e.ExCurrency)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraDate1).HasColumnType("datetime");

                entity.Property(e => e.ExtraDate2).HasColumnType("datetime");

                entity.Property(e => e.FacultativePerc).HasColumnName("facultative_perc");

                entity.Property(e => e.Field1)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field10)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field2)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field3)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field4)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field5)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field6)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field7)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field8)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field9)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.GrossPremium).HasColumnType("money");

                entity.Property(e => e.GrossPremiumBorderaux)
                    .HasColumnName("GrossPremium_Borderaux")
                    .HasColumnType("money");

                entity.Property(e => e.GrossPremiumFrgn).HasColumnType("money");

                entity.Property(e => e.InsuredId)
                    .HasColumnName("InsuredID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.InsuredName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Leader)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.LeaderId)
                    .HasColumnName("LeaderID")
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.MktStaff)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.MktStaffId)
                    .HasColumnName("MktStaffID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MktUnit)
                    .HasColumnName("mktUnit")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.MktUnitId)
                    .HasColumnName("MktUnitID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("smalldatetime");

                entity.Property(e => e.Narration)
                    .HasMaxLength(1028)
                    .IsUnicode(false);

                entity.Property(e => e.NetAmount).HasColumnType("money");

                entity.Property(e => e.NoteType)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.OveriddenOn).HasColumnType("datetime");

                entity.Property(e => e.OveriddingComments).HasMaxLength(255);

                entity.Property(e => e.OverriddenBy).HasMaxLength(255);

                entity.Property(e => e.PackageSubRiskId)
                    .HasColumnName("PackageSubRiskID")
                    .HasMaxLength(255);

                entity.Property(e => e.Party)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.PartyId)
                    .HasColumnName("PartyID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.PartyRate).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.PaymentType)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.PlCode).HasMaxLength(255);

                entity.Property(e => e.Pmlvalue)
                    .HasColumnName("PMLValue")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PolicyNo)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.PostToFinanceBy).HasMaxLength(255);

                entity.Property(e => e.PostToFinanceOn).HasColumnType("datetime");

                entity.Property(e => e.PremiumCollected).HasColumnType("money");

                entity.Property(e => e.ProRataPremium).HasColumnType("money");

                entity.Property(e => e.QuotasharePerc).HasColumnName("quotashare_perc");

                entity.Property(e => e.ReceiptNo)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiptStatus).HasColumnName("Receipt_Status");

                entity.Property(e => e.RefDncnno)
                    .HasColumnName("refDNCNNo")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.RefferenceDncId).HasColumnName("Refference_DncID");

                entity.Property(e => e.Remarks)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.RetPremium).HasColumnType("money");

                entity.Property(e => e.RetProp).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RetValue).HasColumnType("money");

                entity.Property(e => e.RetentionPerc).HasColumnName("retention_perc");

                entity.Property(e => e.Riclass)
                    .HasColumnName("RIClass")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SourceType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("smalldatetime");

                entity.Property(e => e.SubRisk)
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.SubRiskId)
                    .HasColumnName("SubRiskID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.SubmittedBy)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.SubmittedOn).HasColumnType("smalldatetime");

                entity.Property(e => e.SumInsured).HasColumnType("money");

                entity.Property(e => e.SumInsuredFrgn).HasColumnType("money");

                entity.Property(e => e.SurplusPerc).HasColumnName("surplus_perc");

                entity.Property(e => e.SyncDatetime)
                    .HasColumnName("sync_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.TopMostValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TotalPremium).HasColumnType("money");

                entity.Property(e => e.TotalRiskValue).HasColumnType("money");

                entity.Property(e => e.Transguid)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TreatyClaimsAmt).HasColumnType("money");

                entity.Property(e => e.UnderwrittingApprovedBy).HasMaxLength(255);

                entity.Property(e => e.UnderwrittingApprovedOn).HasColumnType("datetime");

                entity.Property(e => e.VatAmount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.VatbatchNumber)
                    .HasColumnName("VATBatchNumber")
                    .HasMaxLength(255);

                entity.Property(e => e.VatcommissionFinalApproved).HasColumnName("VATCommissionFinalApproved");

                entity.Property(e => e.VatcommissionFinalApprovedBy)
                    .HasColumnName("VATCommissionFinalApprovedBy")
                    .HasMaxLength(255);

                entity.Property(e => e.VatcommissionFinalApprovedOn)
                    .HasColumnName("VATCommissionFinalApprovedOn")
                    .HasColumnType("datetime");

                entity.Property(e => e.WithholdingTaxOnCommission).HasColumnType("money");
            });

            modelBuilder.Entity<ExternalPoliciesInterface>(entity =>
            {
                entity.HasKey(e => e.Sequenceid);

                entity.Property(e => e.EmailAddress).HasMaxLength(255);

                entity.Property(e => e.InsuredId).HasColumnName("Insured_ID");

                entity.Property(e => e.MainClassCode).HasMaxLength(255);

                entity.Property(e => e.PolicyNumber).HasMaxLength(255);

                entity.Property(e => e.PortalName).HasMaxLength(255);

                entity.Property(e => e.ProductCode).HasMaxLength(255);

                entity.Property(e => e.ProductName).HasMaxLength(255);

                entity.Property(e => e.Submmittedon)
                    .HasColumnName("submmittedon")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Xmldocument)
                    .HasColumnName("xmldocument")
                    .HasColumnType("xml");
            });

            modelBuilder.Entity<InsuredClients>(entity =>
            {
                entity.HasKey(e => e.InsuredId);

                entity.Property(e => e.InsuredId)
                    .HasColumnName("InsuredID")
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.A1).HasColumnType("money");

                entity.Property(e => e.A2).HasColumnType("money");

                entity.Property(e => e.A3).HasColumnType("money");

                entity.Property(e => e.A4).HasColumnType("money");

                entity.Property(e => e.A5).HasColumnType("money");

                entity.Property(e => e.Address)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.AgentId)
                    .HasColumnName("AgentID")
                    .HasMaxLength(255);

                entity.Property(e => e.AlternativeEmail)
                    .HasColumnName("alternative_email")
                    .HasMaxLength(150);

                entity.Property(e => e.Contperson)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Email).HasMaxLength(250);

                entity.Property(e => e.ExtraDate1).HasColumnType("datetime");

                entity.Property(e => e.ExtraDate2).HasColumnType("datetime");

                entity.Property(e => e.Fax).HasMaxLength(250);

                entity.Property(e => e.Field1)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field10)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field2)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field3)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field4)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field5)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field6)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field7)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field8)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Field9)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasMaxLength(255);

                entity.Property(e => e.GlCode).HasMaxLength(255);

                entity.Property(e => e.InsuredType)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LandPhone).HasMaxLength(250);

                entity.Property(e => e.MeansId)
                    .HasColumnName("MeansID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MeansIdno)
                    .HasColumnName("MeansIDNo")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MktStaffId)
                    .HasColumnName("MktStaffID")
                    .HasMaxLength(255);

                entity.Property(e => e.MobilePhone).HasMaxLength(250);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("smalldatetime");

                entity.Property(e => e.Occupation).HasMaxLength(250);

                entity.Property(e => e.OtherNames)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Passportno)
                    .HasColumnName("passportno")
                    .HasMaxLength(255);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255);

                entity.Property(e => e.PortalInsuredId)
                    .HasColumnName("PortalInsuredID")
                    .HasMaxLength(255);

                entity.Property(e => e.Profile)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.SubmittedBy)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.SubmittedOn).HasColumnType("smalldatetime");

                entity.Property(e => e.Surname)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.TakeOnBalance).HasColumnType("money");

                entity.Property(e => e.Tin)
                    .HasColumnName("tin")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<PolicySchedule>(entity =>
            {
                entity.HasKey(e => e.SequenceId);

                entity.Property(e => e.SequenceId).HasColumnName("SequenceID");

                entity.Property(e => e.A)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.B)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.C).IsRequired();

                entity.Property(e => e.CertificateNo).HasMaxLength(255);

                entity.Property(e => e.CovertypesequenceId).HasColumnName("Covertypesequence_id");

                entity.Property(e => e.Datetimemodified)
                    .HasColumnName("datetimemodified")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeletedReference)
                    .HasColumnName("deleted_reference")
                    .HasMaxLength(255);

                entity.Property(e => e.E)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Effectivedate)
                    .HasColumnName("effectivedate")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Endorsement).HasColumnName("endorsement");

                entity.Property(e => e.EndorsementApproved).HasColumnName("endorsement_approved");

                entity.Property(e => e.Endorsementno)
                    .HasColumnName("endorsementno")
                    .HasMaxLength(255);

                entity.Property(e => e.F).HasMaxLength(250);

                entity.Property(e => e.G).HasMaxLength(250);

                entity.Property(e => e.H).HasMaxLength(250);

                entity.Property(e => e.I).HasMaxLength(250);

                entity.Property(e => e.J).HasMaxLength(250);

                entity.Property(e => e.K).HasMaxLength(250);

                entity.Property(e => e.L).HasMaxLength(250);

                entity.Property(e => e.M).HasMaxLength(250);

                entity.Property(e => e.N).HasMaxLength(250);

                entity.Property(e => e.NiidCanceledBy)
                    .HasColumnName("niid_canceled_by")
                    .HasMaxLength(255);

                entity.Property(e => e.NiidCanceledOn)
                    .HasColumnName("niid_canceled_on")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.NiidUploaded).HasColumnName("niid_uploaded");

                entity.Property(e => e.NiidUploadedBy)
                    .HasColumnName("niid_uploaded_by")
                    .HasMaxLength(255);

                entity.Property(e => e.NiidUploadedOn)
                    .HasColumnName("niid_uploaded_on")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.O).HasMaxLength(250);

                entity.Property(e => e.P).HasMaxLength(250);

                entity.Property(e => e.Policyno)
                    .HasColumnName("policyno")
                    .HasMaxLength(255);

                entity.Property(e => e.Q).HasMaxLength(250);

                entity.Property(e => e.QoutationNo).HasMaxLength(255);

                entity.Property(e => e.R).HasMaxLength(250);

                entity.Property(e => e.S).HasMaxLength(250);

                entity.Property(e => e.Submittedon)
                    .HasColumnName("submittedon")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.T).HasMaxLength(250);

                entity.Property(e => e.U).HasMaxLength(250);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255);

                entity.Property(e => e.V).HasMaxLength(250);

                entity.Property(e => e.W).HasMaxLength(250);

                entity.Property(e => e.X).HasMaxLength(250);

                entity.Property(e => e.Y).HasMaxLength(250);

                entity.Property(e => e.Z).HasMaxLength(250);
            });

            modelBuilder.Entity<QoutationHeader>(entity =>
            {
                entity.HasKey(e => e.QoutationNo);

                entity.Property(e => e.QoutationNo)
                    .HasMaxLength(255)
                    .ValueGeneratedNever();

                entity.Property(e => e.Agent).HasMaxLength(255);

                entity.Property(e => e.ApplicableRateType).HasMaxLength(255);

                entity.Property(e => e.ApprovalDate)
                    .HasColumnName("approvalDate")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Approvedby)
                    .HasColumnName("approvedby")
                    .HasMaxLength(255);

                entity.Property(e => e.Branch)
                    .HasColumnName("branch")
                    .HasMaxLength(255);

                entity.Property(e => e.ClientId)
                    .HasColumnName("ClientID")
                    .HasMaxLength(255);

                entity.Property(e => e.CoinsuranceType).HasMaxLength(255);

                entity.Property(e => e.CommissionAddedBy).HasMaxLength(255);

                entity.Property(e => e.CommissionAddedOn).HasColumnType("datetime");

                entity.Property(e => e.Datetimemodified)
                    .HasColumnName("datetimemodified")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeapprovedBy)
                    .HasColumnName("deapprovedBy")
                    .HasMaxLength(255);

                entity.Property(e => e.Deapprovedon)
                    .HasColumnName("deapprovedon")
                    .HasColumnType("datetime");

                entity.Property(e => e.DebitNoteNumber)
                    .HasColumnName("debit_note_number")
                    .HasMaxLength(255);

                entity.Property(e => e.Effectivate)
                    .HasColumnName("effectivate")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Exchangerate).HasColumnName("exchangerate");

                entity.Property(e => e.Expirydate)
                    .HasColumnName("expirydate")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.FacinReinsurer)
                    .HasColumnName("FACIN_REINSURER")
                    .HasMaxLength(255);

                entity.Property(e => e.LastEndorsementBy).HasMaxLength(255);

                entity.Property(e => e.LastendorsementDate).HasColumnType("datetime");

                entity.Property(e => e.MktStaffId)
                    .HasColumnName("MktStaffID")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Modifiedby)
                    .HasColumnName("modifiedby")
                    .HasMaxLength(255);

                entity.Property(e => e.OriginalPolicyNumber).HasMaxLength(255);

                entity.Property(e => e.OveriddenToProductionBy).HasMaxLength(255);

                entity.Property(e => e.OveriddenToProductionOn).HasColumnType("datetime");

                entity.Property(e => e.PaymentType).HasMaxLength(255);

                entity.Property(e => e.PolicyBizOption)
                    .HasColumnName("Policy_BizOption")
                    .HasMaxLength(255);

                entity.Property(e => e.PolicyClonedFrom)
                    .HasColumnName("PolicyCloned_From")
                    .HasMaxLength(255);

                entity.Property(e => e.PolicyPrintedBy).HasMaxLength(255);

                entity.Property(e => e.PolicyScheduleType).HasMaxLength(255);

                entity.Property(e => e.PolicySumInsured).HasColumnType("money");

                entity.Property(e => e.Policyno)
                    .HasColumnName("policyno")
                    .HasMaxLength(255);

                entity.Property(e => e.PortalName).HasMaxLength(255);

                entity.Property(e => e.PremiumCollectionType).HasMaxLength(255);

                entity.Property(e => e.PreviousPolicyNumber).HasMaxLength(255);

                entity.Property(e => e.QoutationCreator)
                    .HasColumnName("Qoutation_Creator")
                    .HasMaxLength(255);

                entity.Property(e => e.QoutationCurrency).HasMaxLength(255);

                entity.Property(e => e.QoutationDatetime)
                    .HasColumnName("Qoutation_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.QoutationId)
                    .HasColumnName("QoutationID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.QoutationStatus).HasMaxLength(255);

                entity.Property(e => e.QouteEmailAddress)
                    .HasColumnName("Qoute_EmailAddress")
                    .HasMaxLength(255);

                entity.Property(e => e.QouteInsuredName)
                    .HasColumnName("Qoute_InsuredName")
                    .HasMaxLength(255);

                entity.Property(e => e.QouteMobileNumber)
                    .HasColumnName("Qoute_MobileNumber")
                    .HasMaxLength(255);

                entity.Property(e => e.ReceiptedBy).HasMaxLength(255);

                entity.Property(e => e.ReceiptedOn).HasColumnType("smalldatetime");

                entity.Property(e => e.Receiptedamount)
                    .HasColumnName("receiptedamount")
                    .HasColumnType("money");

                entity.Property(e => e.Receiptnumber)
                    .HasColumnName("receiptnumber")
                    .HasMaxLength(255);

                entity.Property(e => e.RefCreditNoteDate).HasColumnType("smalldatetime");

                entity.Property(e => e.RefCreditNoteNumber).HasMaxLength(255);
            });

            modelBuilder.Entity<QoutationItemsPremium>(entity =>
            {
                entity.HasKey(e => e.Sequenceid);

                entity.ToTable("QoutationItems_Premium");

                entity.Property(e => e.CoverTypeIndicatorId).HasColumnName("CoverTypeIndicatorID");

                entity.Property(e => e.CovertedAmount).HasColumnType("money");

                entity.Property(e => e.DateTimeModified)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeletedReference)
                    .HasColumnName("deleted_reference")
                    .HasMaxLength(255);

                entity.Property(e => e.Doublerate)
                    .HasColumnName("doublerate")
                    .HasMaxLength(250);

                entity.Property(e => e.Endorsedby)
                    .HasColumnName("endorsedby")
                    .HasMaxLength(255);

                entity.Property(e => e.Endorsement).HasColumnName("endorsement");

                entity.Property(e => e.EndorsementApproved).HasColumnName("endorsement_approved");

                entity.Property(e => e.EndorsementApprovedon)
                    .HasColumnName("endorsement_approvedon")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndorsementDebitNote)
                    .HasColumnName("endorsement_debit_note")
                    .HasMaxLength(255);

                entity.Property(e => e.EndorsementEffectiveDate)
                    .HasColumnName("Endorsement_EffectiveDate")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EndorsementNo).HasMaxLength(255);

                entity.Property(e => e.EndorsementPaymentType)
                    .HasColumnName("Endorsement_PaymentType")
                    .HasMaxLength(255);

                entity.Property(e => e.EndorsementRefCreditNoteDate)
                    .HasColumnName("Endorsement_RefCreditNoteDate")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.EndorsementRefCreditNoteNumber)
                    .HasColumnName("Endorsement_RefCreditNoteNumber")
                    .HasMaxLength(255);

                entity.Property(e => e.Endorsementreason)
                    .HasColumnName("endorsementreason")
                    .HasMaxLength(255);

                entity.Property(e => e.ExcessAmount)
                    .HasColumnName("Excess_Amount")
                    .HasColumnType("money");

                entity.Property(e => e.InsuredMemberId).HasColumnName("InsuredMemberID");

                entity.Property(e => e.InsuredName).HasMaxLength(255);

                entity.Property(e => e.PremiumAmount).HasColumnType("money");

                entity.Property(e => e.PreviousSuminsured)
                    .HasColumnName("previous_suminsured")
                    .HasColumnType("money");

                entity.Property(e => e.QoutationNo).HasMaxLength(255);

                entity.Property(e => e.QouteGroupIndicatorId).HasColumnName("Qoute_GroupIndicatorID");

                entity.Property(e => e.Rate1).HasColumnName("rate1");

                entity.Property(e => e.Rate2).HasColumnName("rate2");

                entity.Property(e => e.Rate3).HasColumnName("rate3");

                entity.Property(e => e.RiskGroup).HasMaxLength(255);

                entity.Property(e => e.RiskTableCategory).HasMaxLength(255);

                entity.Property(e => e.StaffId)
                    .HasColumnName("staffID")
                    .HasMaxLength(255);

                entity.Property(e => e.SumInsured).HasColumnType("money");

                entity.Property(e => e.TppdAllowedAmount)
                    .HasColumnName("tppd_allowed_amount")
                    .HasColumnType("money");

                entity.Property(e => e.Tppdunits).HasColumnName("tppdunits");
            });

            modelBuilder.Entity<QoutationRiskDistributions>(entity =>
            {
                entity.HasKey(e => e.SequenceId);

                entity.ToTable("Qoutation_RiskDistributions");

                entity.Property(e => e.SequenceId).HasColumnName("SequenceID");

                entity.Property(e => e.CessionNumber).HasMaxLength(255);

                entity.Property(e => e.ClaimAdjustorFeePaid).HasColumnType("money");

                entity.Property(e => e.ClaimPaidAmount).HasColumnType("money");

                entity.Property(e => e.CommissionAmount).HasColumnType("money");

                entity.Property(e => e.DatetimeModified).HasColumnType("smalldatetime");

                entity.Property(e => e.ExternalPolicyNo).HasMaxLength(255);

                entity.Property(e => e.FacReferenceNumber).HasMaxLength(255);

                entity.Property(e => e.Facultative).HasColumnType("money");

                entity.Property(e => e.FacultativePremiumAmount).HasColumnType("money");

                entity.Property(e => e.FirstLossAmount).HasMaxLength(255);

                entity.Property(e => e.GrossPremium).HasColumnType("money");

                entity.Property(e => e.GrossPremiumBorderaux)
                    .HasColumnName("GrossPremium_Borderaux")
                    .HasColumnType("money");

                entity.Property(e => e.InsuranceEndDate).HasColumnType("smalldatetime");

                entity.Property(e => e.InsuranceStartDate).HasColumnType("smalldatetime");

                entity.Property(e => e.InsuredName).HasMaxLength(255);

                entity.Property(e => e.LimitPerCarriage).HasMaxLength(255);

                entity.Property(e => e.LocationType).HasMaxLength(255);

                entity.Property(e => e.MarineRiskClass).HasMaxLength(255);

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OurRetentionPremiumAmount).HasColumnType("money");

                entity.Property(e => e.PolicyNo).HasMaxLength(255);

                entity.Property(e => e.PolicyTypeId).HasColumnName("PolicyTypeID");

                entity.Property(e => e.QoutaShareAmount).HasColumnType("money");

                entity.Property(e => e.QoutationNo).HasMaxLength(255);

                entity.Property(e => e.ReinsuranceEndDate).HasColumnType("smalldatetime");

                entity.Property(e => e.ReinsuranceProcessedBy).HasMaxLength(255);

                entity.Property(e => e.ReinsuranceReferenceNumber).HasMaxLength(255);

                entity.Property(e => e.ReinsuranceStartDate).HasColumnType("smalldatetime");

                entity.Property(e => e.Remarks)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.RetentionAmount).HasColumnType("money");

                entity.Property(e => e.RiskClass).HasMaxLength(255);

                entity.Property(e => e.ScheduleType).HasMaxLength(255);

                entity.Property(e => e.SubClass).HasMaxLength(255);

                entity.Property(e => e.SubRiskClass).HasMaxLength(255);

                entity.Property(e => e.Surplus).HasColumnType("money");

                entity.Property(e => e.Topmostlocation)
                    .HasColumnName("topmostlocation")
                    .HasColumnType("money");

                entity.Property(e => e.TotalSumInsured).HasColumnType("money");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255);

                entity.Property(e => e.Vatamount)
                    .HasColumnName("VATAmount")
                    .HasColumnType("money");

                entity.Property(e => e.Vatpercentage).HasColumnName("VATPercentage");
            });

            modelBuilder.Entity<SystemDefaults>(entity =>
            {
                entity.HasKey(e => e.NextInsuredId);

                entity.ToTable("SYSTEM_DEFAULTS");

                entity.Property(e => e.NextInsuredId)
                    .HasColumnName("NEXT_INSURED_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Basecurrency)
                    .IsRequired()
                    .HasColumnName("basecurrency")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("('NAIRA')");

                entity.Property(e => e.MaximumCommissionRate).HasDefaultValueSql("((20))");

                entity.Property(e => e.NextClaimNo)
                    .HasColumnName("next_claim_no")
                    .HasDefaultValueSql("((1000))");

                entity.Property(e => e.NextCommissionBatch)
                    .HasColumnName("Next_Commission_Batch")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.NextCommissionVatBatch)
                    .HasColumnName("Next_CommissionVAT_Batch")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.NextCreditNo)
                    .HasColumnName("next_credit_no")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.NextDebitNo).HasColumnName("next_debit_no");

                entity.Property(e => e.NextDuplicateBatch).HasColumnType("numeric(20, 0)");

                entity.Property(e => e.NextMarineCertificate)
                    .HasColumnName("next_marine_certificate")
                    .HasDefaultValueSql("((200000000))");

                entity.Property(e => e.NextMotorCertificate)
                    .HasColumnName("Next_Motor_Certificate")
                    .HasDefaultValueSql("((200000000))");

                entity.Property(e => e.NextPartyId)
                    .HasColumnName("next_party_id")
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((7000000))");

                entity.Property(e => e.NextPolicyId)
                    .HasColumnName("next_policy_id")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.NiidUser)
                    .HasColumnName("niid_user")
                    .HasMaxLength(255);

                entity.Property(e => e.NiidUserPassword)
                    .HasColumnName("niid_user_password")
                    .HasMaxLength(255);

                entity.Property(e => e.TppdDefaultValue)
                    .HasColumnName("tppd_default_value")
                    .HasColumnType("money")
                    .HasDefaultValueSql("((1000000))");
            });

            modelBuilder.Entity<VehMakeBrands>(entity =>
            {
                entity.HasKey(e => e.BrandId);

                entity.Property(e => e.BrandId).HasColumnName("BrandID");

                entity.Property(e => e.Brand)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.Make)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MakeId).HasColumnName("MakeID");
            });

            modelBuilder.Entity<VehMakes>(entity =>
            {
                entity.HasKey(e => e.MakeId);

                entity.Property(e => e.MakeId).HasColumnName("MakeID");

                entity.Property(e => e.Make)
                    .HasColumnName("make")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Tag)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
        }
    }
}
