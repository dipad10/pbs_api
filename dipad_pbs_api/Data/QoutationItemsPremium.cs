﻿using System;
using System.Collections.Generic;

namespace Dipad_PBS_API.Data
{
    public partial class QoutationItemsPremium
    {
        public int Sequenceid { get; set; }
        public string QoutationNo { get; set; }
        public int? PackageCoverTypeSequenceid { get; set; }
        public int? CoverTypeSequenceid { get; set; }
        public int? CoverTypeIndicatorId { get; set; }
        public double? Rate1 { get; set; }
        public double? Rate2 { get; set; }
        public double? Rate3 { get; set; }
        public string Doublerate { get; set; }
        public int? NumberOfUnits { get; set; }
        public decimal? SumInsured { get; set; }
        public decimal? PremiumAmount { get; set; }
        public string DeletedReference { get; set; }
        public int? InsuredMemberId { get; set; }
        public int? InsuredDependent { get; set; }
        public int? MemberSequence { get; set; }
        public string InsuredName { get; set; }
        public int? InsuredAge { get; set; }
        public string StaffId { get; set; }
        public DateTime DateTimeModified { get; set; }
        public string RiskGroup { get; set; }
        public string RiskTableCategory { get; set; }
        public decimal? ExcessAmount { get; set; }
        public double? ExcessPercentage { get; set; }
        public double? ExchangeRate { get; set; }
        public decimal? CovertedAmount { get; set; }
        public int QouteGroupIndicatorId { get; set; }
        public int Tppdunits { get; set; }
        public decimal TppdAllowedAmount { get; set; }
        public bool Endorsement { get; set; }
        public bool EndorsementApproved { get; set; }
        public string EndorsementNo { get; set; }
        public DateTime? EndorsementEffectiveDate { get; set; }
        public decimal? PreviousSuminsured { get; set; }
        public DateTime? EndorsementApprovedon { get; set; }
        public string Endorsedby { get; set; }
        public string Endorsementreason { get; set; }
        public string EndorsementDebitNote { get; set; }
        public string EndorsementRefCreditNoteNumber { get; set; }
        public DateTime? EndorsementRefCreditNoteDate { get; set; }
        public string EndorsementPaymentType { get; set; }
        public bool ShortPeriod { get; set; }
        public int RemainingPeriod { get; set; }
    }
}
