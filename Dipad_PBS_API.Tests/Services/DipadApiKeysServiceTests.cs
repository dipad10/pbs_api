using Xunit;
using System;
using System.Collections.Generic;
using System.Linq;
using Dipad_PBS_API;
using Dipad_PBS_API.Services;
using Dipad_PBS_API.Data;

namespace Dipad_PBS_API.Tests.Services
{
   
    public class DipadApiKeysServiceTests
    {
        [Fact]
        public void GetKeys_ShouldReturnListOfApiKeys()
        {
                //Arrange
                DipadApiKeys_Service service = new DipadApiKeys_Service();
                //Act
                var keys = service.GetKeys();
                //Assert
                Assert.True(keys.Any());
        }
         [Theory]
         [InlineData("bg5885-74984-gj090-jhf89")]
        public void ValidateKey_ShouldReturnValidKey(string key)
        {
              //Arrange
              DipadApiKeys_Service service = new DipadApiKeys_Service();
              //Act
              var Apikey = service.ValidateKey(key);
              //Assert
              Assert.Equal(Apikey.ApiKey, key);
        }
        
        [Theory]
        [InlineData("bg588900-74984-gj090-jhf89")]
        public void ValidateKey_ShouldReturnNullIfNoKeyFound(string key)
        {
            //Arrange
            DipadApiKeys_Service service = new DipadApiKeys_Service();
            //Act
            var Apikey = service.ValidateKey(key);
            //Assert
            Assert.Null(Apikey);
        }
        [Fact]
         public void Generatekey_ShouldReturnAKey()
                {
                        //Arrange
                        DipadApiKeys_Service service = new DipadApiKeys_Service();
                        //Act
                        var key = service.Generatekey();
                        //Assert
                        Assert.Single(key);
                }
    }
}